﻿using UnityEngine;
using System.Collections;
using Nimbi.UI;



namespace Nimbi.Interaction.Triggers
{
    public class MerchantessTrigger : MonoBehaviour
    {

        public string market;
        public string marketPlace;
        public string lightHouse;
        public string carpet;

        public float dialogueLength;

        bool firstInteraction = false;
  

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player" && !firstInteraction)
            {
                DialogueManager.Add(market.Localize(), dialogueLength);
                DialogueManager.Add(marketPlace.Localize(), dialogueLength);
                DialogueManager.Add(lightHouse.Localize(), dialogueLength);
                DialogueManager.Add(carpet.Localize(), dialogueLength);

                firstInteraction = true;
            }
        }

    }
}

