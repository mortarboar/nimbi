﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Interaction.Triggers
{
    public class LightPlacementTrigger : MonoBehaviour
    {

        private GameObject player;
        public GameObject particle;
        public GameObject lighthouseLight;
        public EndBlackFader fader;
        public GameObject lightHolder;
        public GameObject spotlight;
        public TowerRotator towerRotator;

        private bool lightPlaced;
        private bool endAnimation;

        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        void Update()
        {
            if (lightPlaced && towerRotator.locked)
            {
                StartCoroutine("EndFader");
                endAnimation = true;
                spotlight.SetActive(true);
            }
            if (endAnimation)
            {
                Camera.main.transform.localPosition = new Vector3(0, 0, Camera.main.transform.localPosition.z - 6f * Time.deltaTime);
                lightHolder.transform.localRotation = Quaternion.Euler(lightHolder.transform.localRotation.eulerAngles + new Vector3(0, 30, 0) * Time.deltaTime);
            }
        }

        private void OnTriggerStay(Collider coll)
        {
            if (coll.tag == "Player")
            {
                if (Input.GetButtonDown("Submit"))
                {
                    foreach (Transform child in player.transform)
                    {
                        if (child.name == "lighthouse_light")
                        {
                            Transform _child = child;
                            _child.parent = GameObject.Find("lighthouse_pole").transform;
                            _child.transform.localPosition = new Vector3(0, 3.6f, -.5f);
                            _child.transform.localScale = new Vector3(1, 1, 1);

                            //Disable our Particle and Activate our Light GameObject!
                            particle.SetActive(false);
                            lightPlaced = true;
                        }
                    }
                }
            }
        }

        IEnumerator EndFader()
        {
            yield return new WaitForSeconds(9.0f);
            fader.BeginFade();
        }
    }
}