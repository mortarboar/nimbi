﻿using UnityEngine;
using Nimbi.Actors;

namespace Nimbi.Interaction.Triggers
{
    public class WindForceTrigger : MonoBehaviour
    {
        public GameObject attachedObject;

        private Player player;
        private float force = 30f;
        private float forceObject = 2f;
        private bool playerInside = false;
        private bool objectInWind = false;

        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        void FixedUpdate()
        {
            Wind();
        }

        private void Wind()
        {
            if (playerInside && !Player.Onesie.isHeavy)
                player.GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Acceleration);
            if (objectInWind && !attachedObject.GetComponent<ObjectTrigger>().isTriggered)
                attachedObject.GetComponent<Rigidbody>().AddForce(transform.forward * forceObject, ForceMode.Acceleration);
        }

        private void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
                playerInside = true;
            if (coll.gameObject == attachedObject)
                objectInWind = true;
        }

        private void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
                playerInside = false;
            if (coll.gameObject == attachedObject)
                objectInWind = false;
        }
    }
}