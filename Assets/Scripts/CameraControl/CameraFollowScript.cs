﻿using System.Linq;
using UnityEngine;

namespace Nimbi.CameraControl
{
	public class CameraFollowScript : MonoBehaviour
    {
        private GameObject cameraObject;
        private Transform startTransform;
		private GameObject focus;
        public bool canZoom = true;
        private float zoomLevel = -15f;

		public void Start()
        {
            cameraObject = GetComponentInChildren<Camera>().gameObject;
            startTransform = gameObject.transform;
            focus = GameObject.FindGameObjectWithTag("Player");
		}

		void Update()
		{
            // Mousewheel zoom (should probably rename mouse wheel axis to "Zoom" at some point)
			if (Input.GetAxis("Mouse Wheel") != 0 && canZoom) {
                cameraObject.transform.localPosition = new Vector3(0, 0, Mathf.Max(-15,
                    Mathf.Min(cameraObject.transform.localPosition.z + Input.GetAxis("Mouse Wheel"), -5)));
            }

            // Controller zoom
            if ((Input.GetAxis("Zoom") > 0.05f || Input.GetAxis("Zoom") < -0.05f) && canZoom) {
                bool zoomIn = Input.GetAxis("Zoom") > 0;

                zoomLevel = Mathf.Max(-15, Mathf.Min(cameraObject.transform.localPosition.z + (zoomIn ? 1 : -1), -5));

                cameraObject.transform.localPosition = new Vector3(0, 0, zoomLevel);
            }

            transform.position = new Vector3(focus.transform.position.x, focus.transform.position.y + 0.5f, focus.transform.position.z);

            //Collision check with objects in scene
            /*RaycastHit[] hits;
            if ((hits = CheckCameraCollisions(focus.transform.position)).Length != 0)
            {
                RaycastHit hit = hits.First<RaycastHit>();
                cameraObject.transform.localPosition = Vector3.MoveTowards(cameraObject.transform.localPosition, new Vector3(0, 0, Mathf.Max(-15, Mathf.Min(hit.distance * -1, -2))), 8f*Time.deltaTime);
                Debug.Log(hit.distance);
            }
            else
                cameraObject.transform.localPosition = Vector3.MoveTowards(cameraObject.transform.localPosition, new Vector3(0, 0, zoomLevel), 12f * Time.deltaTime);*/
        }

        public Transform getStartTransform() { return startTransform; }

		public void SetCameraFocus(GameObject _focus) { focus = _focus; }

        private RaycastHit[] CheckCameraCollisions(Vector3 origin)
        {
            RaycastHit[] hits;
            Vector3 direction = cameraObject.transform.position - origin;
            direction = direction.normalized;
            hits = Physics.RaycastAll(origin, direction, 15f).Where(h => h.collider.tag != "Player" && h.collider.tag != "Grass").OrderBy(h => h.distance).ToArray();
            return hits;
        }
	}
}