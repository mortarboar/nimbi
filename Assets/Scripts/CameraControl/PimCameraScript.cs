﻿using UnityEngine;
using Nimbi.Actors;
using UnityEngine.UI;

namespace Nimbi.CameraControl
{
	public class PimCameraScript : MonoBehaviour
	{
		private Transform centerFocus;
		public float degree;
		public float verticalDegree = 30;
		public float cameraSpeed = 6;
        private bool animationPlayed;
        public bool cameraEnabled = true; // Boolean to call when Nimbi can Interact with the Camera.
        private float deadzone = 0.1f;

        private GameObject player;

		#region Properties
		public float Sensitivity
		{
			get { return PlayerPrefs.GetFloat("CameraSensitivity", 2f); }
		}
		#endregion


		void Start()
		{
			centerFocus = GameObject.Find("CenterFocus").transform;
            player = GameObject.FindGameObjectWithTag("Player");
        }

		void Update()
		{


			// Rotate controls
			if ((Input.GetMouseButton(0) || Input.GetMouseButton(1)) && cameraEnabled) //Disable Camera for Introduction Level
			{
				degree += Input.GetAxis("Mouse X") * Sensitivity * 1.2f;
			}
			else if ((Input.GetAxis("RightJoystick") != 0))
			{
                if(Mathf.Abs(Input.GetAxis("RightJoystick")) > deadzone)
				    degree += Input.GetAxis("RightJoystick") * Sensitivity;
			}

			degree = degree % 360;

			//Set rotation to next degree with a slight lerp
            if (cameraEnabled)
			    centerFocus.rotation = Quaternion.Slerp(centerFocus.rotation, Quaternion.Euler(verticalDegree, degree, 0), Time.deltaTime * cameraSpeed);
		}
	}
}