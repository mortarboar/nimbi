﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class NewWater : MonoBehaviour {

    bool inTrigger;

    public GameObject watersplash;
    Vector3 lastPlayerPosition;
    public GameObject player;
	
	// Update is called once per frame
	void Update () {
        if (inTrigger && Vector3.Distance(lastPlayerPosition, player.transform.position) > 0.05f)
        {
            GameObject particle = Instantiate(watersplash, player.transform.position, Quaternion.identity) as GameObject;
            Destroy(particle, 1.0f);
            lastPlayerPosition = player.transform.position;
        }
	}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player")
            inTrigger = true;
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "Player")
            inTrigger = false;
    }

}