﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Nimbi.Actors
{
    public class WallCollision : MonoBehaviour
    {
        Vector3[] Directions = { Vector3.right, Vector3.left, Vector3.forward, Vector3.back };
        void Update()
        {
            foreach(Vector3 dir in Directions)
            {
                bool wallHit = false;
                RaycastHit[] hits = Physics.RaycastAll(transform.position + new Vector3(0, 0.5f, 0), dir, 0.45f).Where(h => h.collider.tag != "Player").OrderBy(h => h.distance).ToArray();
                foreach(RaycastHit hit in hits)
                {
                    if (hit.collider.tag == "Wall")
                        wallHit = true;
                }
                Debug.DrawRay(transform.position + new Vector3(0, 0.5f, 0), dir, wallHit?Color.green:Color.red);
            }
        }
    }
}