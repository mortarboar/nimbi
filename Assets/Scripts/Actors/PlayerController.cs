﻿using Nimbi.UI;
using Nimbi.VFX;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Nimbi.Actors
{
    //We need the following components to make the player work
    [RequireComponent(typeof(Player))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(TrailRenderer))]
    [RequireComponent(typeof(AudioSource))]
    public class PlayerController : MonoBehaviour
    {
        private static readonly float InputDelay = 1;

        //The actual player with all the movement properties
        private BoxCollider box;
        private CapsuleCollider capsule;
        private HeadsUpDisplayScript hudUi;
        private Transform mainCamera;
        private Player player;

        //Moving variables
        private Vector3 movement;
        private float turnAmount;
        private float forwardAmount;
        private float movingTurnSpeed = 360;
        private bool removeChargeOnNextFrame;
        private float stationaryTurnSpeed = 360;
        private float switchDelay;
        private Vector3 groundNormal;

        public bool canMove = true;
        
        private bool hamsterWheelMovement;
        
        private bool moving;
        private float abilityCooldown;
        private OnesieSwitch switchAnimation;
        private float lastMoved;
        private bool sitting;
        private float nextHeadMovement;
        private AudioSource walksound;
          

        public AudioClip regularWalkSound;
        public AudioClip grassWalkSound;
        private int inGrass;
        private float deadzone = 0.1f;

        private Vector3[] Directions = { Vector3.right, Vector3.left, Vector3.forward, Vector3.back };

        private void Awake()
        {
            this.mainCamera = Camera.main.transform;
            this.player = GetComponent<Player>();
            this.switchAnimation = FindObjectOfType<OnesieSwitch>();
            lastMoved = Time.time;
            walksound = GetComponent<AudioSource>();
        }

        private void Start()
        {
            walksound.Stop();
        }

        private void FixedUpdate()
        {
            //Get Input controls
            float h = Mathf.Abs(CrossPlatformInputManager.GetAxisRaw("Horizontal")) < deadzone ? 0 : CrossPlatformInputManager.GetAxis("Horizontal");
            float v = Mathf.Abs(CrossPlatformInputManager.GetAxisRaw("Vertical")) < deadzone ? 0 : CrossPlatformInputManager.GetAxis("Vertical");


            bool onesie1 = CrossPlatformInputManager.GetButton("Onesie 1");
            bool onesie2 = CrossPlatformInputManager.GetButton("Onesie 2");
            bool onesie3 = CrossPlatformInputManager.GetButton("Onesie 3");
            bool submit = CrossPlatformInputManager.GetButton("Submit");

            if (switchDelay > 0)
            {
                switchDelay -= Time.smoothDeltaTime;
            }

            if (player.PlayerCanSwitchOnesie)
            {
                if ((onesie1 || onesie2 || onesie3) && switchDelay <= 0)
                {
                    int input = onesie1 ? 0 : (onesie2 ? 1 : 2);

                    if ((input == 0 && player.HasOnesie(OnesieType.Elephant)) ||
                        (input == 1 && player.HasOnesie(OnesieType.Hamster)) ||
                        (input == 2 && player.HasOnesie(OnesieType.Dragon)))
                    {
                        //Switch to the selected onesie
                        switchAnimation.Play(input);
                        switchDelay = InputDelay;
                        sitting = false;
                        lastMoved = Time.time;
                    }

                }
            }

            if (canMove)
            {
                //Apply movement, jumping and rotation
                movement = new Vector3(h, 0f, v);
                Move(movement);
            }
            
            //If HamsterWheelMovement is Enabled!
            if(hamsterWheelMovement){
                    //Apply movement, jumping and rotation
                    movement = new Vector3(0, 0f, 0);
                    Move(movement);
            }
         

            if (submit)
            {
                // Use onesie special ability if it has one
                if (abilityCooldown <= 0)
                {
                    abilityCooldown = 0.65f;
                    player.UseOnesieSpecialAbility();
                }
            }

            abilityCooldown -= Time.deltaTime;
            if (canMove)
                player.Animator.SetBool("Moving", h != 0 || v != 0);

            if (h != 0 || v != 0)
            {
                lastMoved = Time.time;
                sitting = false;
                player.Animator.ResetTrigger("Sit");
                player.Animator.ResetTrigger("Move Head");
                if (!walksound.isPlaying)
                    walksound.Play();
            }
            else
            {
                walksound.Stop();
            }

            if (lastMoved + 10f < Time.time)
            {
                if (!sitting)
                {
                    player.Animator.SetTrigger("Sit");
                    sitting = true;
                    nextHeadMovement = Time.time + Random.Range(6f, 12f);
                }
                if (sitting && nextHeadMovement < Time.time)
                {
                    player.Animator.SetTrigger("Move Head");
                    nextHeadMovement = Time.time + Random.Range(15f, 20f);
                }
            }


        }

        private void Move(Vector3 movement)
        {
            //Get camera position to face walking directory
            movement = mainCamera.transform.TransformDirection(movement);

            //If the magintude goes higher then 1, bring it back to 1
            //Magnitude is the length between the vectors origin and its endpoint
            if (movement.magnitude > 1f) movement.Normalize();

            //Invert direction - http://docs.unity3d.com/ScriptReference/Transform.InverseTransformDirection.html
            movement = transform.InverseTransformDirection(movement);

            //Check if the player is grounded
            IsGrounded();

            //Stick the model to the surface
            movement = Vector3.ProjectOnPlane(movement, groundNormal);

            //Calculate turning amount based on ???
            turnAmount = Mathf.Atan2(movement.x, movement.z);

            forwardAmount = movement.z;

            //Turn the character
            TurnRotation();

            Debug.DrawRay(transform.position, transform.TransformDirection(movement * Player.Onesie.movementSpeed)*4, Color.green);

            foreach (Vector3 dir in Directions)
            {
                bool wallHit = false;
                RaycastHit[] hits = Physics.RaycastAll(transform.position + new Vector3(0, 0.3f, 0), dir, 0.4f).Where(h => h.collider.tag != "Player").OrderBy(h => h.distance).ToArray();
                foreach (RaycastHit hit in hits)
                {
                    if (hit.collider.tag == "Wall")
                        wallHit = true;
                }
                if (wallHit)
                {
                    if (dir == Vector3.right && transform.TransformDirection(movement * Player.Onesie.movementSpeed).x > 0)
                        movement.x = 0;
                    else if (dir == Vector3.left && transform.TransformDirection(movement * Player.Onesie.movementSpeed).x < 0)
                        movement.x = 0;
                    else if (dir == Vector3.forward && transform.TransformDirection(movement * Player.Onesie.movementSpeed).z > 0)
                        movement.z = 0;
                    else if (dir == Vector3.back && transform.TransformDirection(movement * Player.Onesie.movementSpeed).z < 0)
                        movement.z = 0;
                }
            }

            //Translate the current position, based on the movementspeed / time to move the player
            GetComponent<Rigidbody>().MovePosition(transform.position + transform.TransformDirection(movement * Player.Onesie.movementSpeed * Time.deltaTime));
        }

        private void TurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
            transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
        }

        private void IsGrounded()
        {
            RaycastHit hitInfo;

            // 0.1f is a small offset to start the ray from inside the character
            // it is also good to note that the transform position in the sample assets is at the base of the character
            if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, 0.1f))
            {
                groundNormal = hitInfo.normal;
            }
            else
            {
                groundNormal = Vector3.up;
            }
        }

        public void EnableMovement()
        {
            if (!canMove)
            {
                canMove = true;
            }
        }
        public void DisableMovement()
        {
            if (canMove)
            {
                canMove = false;
            }
        }
        
        public void HamsterWheelMovement(){
           hamsterWheelMovement = true;
        }
        

        public bool GetMovement()
        {
            return canMove;
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Grass")
            {
                walksound.clip = grassWalkSound;
                inGrass += 1;
            }
        }

        void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Grass")
            {
                inGrass -= 1;
                if (inGrass <= 0)
                    walksound.clip = regularWalkSound;
            }
        }
    }
}
