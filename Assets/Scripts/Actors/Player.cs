﻿using Nimbi.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Nimbi.Interaction;

namespace Nimbi.Actors
{
    public class Player : MonoBehaviour
    {
        private float charge;
        private static Onesie currentOnesie;
        private Skeleton currentSkeleton;
        private static Onesie defaultOnesie;
        private static Dictionary<string,Onesie> onesies = new Dictionary<string,Onesie>();
        private List<GameObject> particleSystems;
        private List<Skeleton> skeletons;
        
        private bool canPickup;
        private bool canInteract;
        
        
        
        private GameObject Holdable;
        private GameObject Interactable;
        
        private PickableHandler pickUp;

        public int Charges;
        
        
        public GameObject InteractButton;

        public bool PlayerCanSwitchOnesie { get; set; }

        public Animator Animator
        {
            get { return currentSkeleton.GetComponent<Animator>(); }
        }

        public bool HoldingWater
        {
            get;
            set;
        }

        public static Onesie Onesie
        {
            get { return currentOnesie == null ? defaultOnesie : currentOnesie; }
            set { currentOnesie = onesies.ContainsKey(value.type.ToString()) ? value : currentOnesie; }
        }

        public static Dictionary<string,Onesie> Onesies
        {
            get { return onesies; }
        }

        public float StaticCharge
        {
            get { return charge; }
            set
            {
                charge = Mathf.Max(0, Mathf.Min(value, 100));
                if (charge > 90)
                {
                    charge = 0;
                    Charges = 3;
                }
            }
        }

        public void AddOnesie(OnesieType type)
        {
            PlayerPrefs.SetInt("Onesie_" + type.ToString(), 1);
            
        }

        public void Awake()
        {
            PlayerCanSwitchOnesie = true;
            defaultOnesie = new HumanOnesie();
            GiveOnesies();
            particleSystems = GameObject.Find("Nimbi/VFX").transform.Cast<Transform>()
                .Where(t => t.GetComponent<ParticleSystem>() != null)
                .Select(t => t.gameObject).ToList();
            skeletons = GetComponentsInChildren<Skeleton>().ToList();
        }
            
        public void ResetRenderers()
        {
            currentSkeleton.Renderer.enabled = true;
            skeletons.Where(s => s != currentSkeleton).ToList().ForEach(s => s.Renderer.enabled = false);
        }

        public void SetEffectActive(string name, bool active)
        {
            particleSystems.Single(g => g.name == name).SetActive(active);
        }

        public IEnumerator SetEffectActiveForDuration(string name, float duration)
        {
            ParticleSystem effect = particleSystems.Single(g => g.name == name).GetComponent<ParticleSystem>();
            float t = 0;

            effect.gameObject.SetActive(true);
            effect.loop = true;
            effect.Play();

            while (t < duration)
            {
                t += Time.deltaTime;
                yield return null;
            }

            effect.gameObject.SetActive(false);
            effect.loop = false;
            effect.Stop();
        }

        private void SetSkeleton(string name)
        {
            currentSkeleton = skeletons.Single(s => s.name == "Onesie" + name);
            currentSkeleton.GetComponent<Animator>().SetTrigger("To_" + name);
            currentSkeleton.Renderer.enabled = true;
            skeletons.Where(s => s != currentSkeleton).ToList().ForEach(s => s.Renderer.enabled = false);
        }

        public void Start()
        {
            Holdable = null;
            Physics.gravity = new Vector3(0, -35, 0);
            SetSkeleton("Default");


            

            if (Debug.isDebugBuild)
            {
                AddOnesie(OnesieType.Dragon);
                AddOnesie(OnesieType.Elephant);
                AddOnesie(OnesieType.Hamster);
            }
        }

    

        public void SwitchOnesie(OnesieType t)
        {
            Debug.Log(t.ToString());
            if (GetComponent<PlayerController>().canMove && onesies.ContainsKey(t.ToString()))
            {
                try
                {
                    currentOnesie = (currentOnesie == onesies[t.ToString()]) ? defaultOnesie : onesies[t.ToString()];
                    AudioManager.Instance.PlaySoundeffect(AudioManager.Instance.GetOnesieSwitchSound(currentOnesie.type.ToString()));
                    SetSkeleton(currentOnesie.type.ToString());
                    if (GetComponent<TrailRenderer>() != null && currentOnesie.type == OnesieType.Hamster)
                        GetComponent<TrailRenderer>().enabled = true;
                    else if (GetComponent<TrailRenderer>() != null)
                        GetComponent<TrailRenderer>().enabled = false;
                }
                catch (Exception ex)
                {
                    Debug.Log("Error in Player.cs: " + ex.Message + ", \r\nTrace: " + ex.StackTrace);
                    SetSkeleton("Default");
                }
            }
        }


        //An Collision Detector With Tags?
        private void OnTriggerEnter(Collider coll){

            //If our Player makes Collision with an Item that needs to be Picked up!
            if (coll.tag == "Pickable"){
                canPickup = true;
                Holdable = coll.gameObject;
                InteractButton.SetActive(true);
                Debug.Log("This is an " + Holdable + " that we can Pick Up");
                
                
            }

            //If our player makes Collission with an Interactor like NPC.
            if (coll.tag == "Interactable")
            {
                Interactable = coll.gameObject;
                InteractButton.SetActive(true);
                Debug.Log("This is " + Interactable + " that we can Interact with");
                canInteract = true;
            
            }

            if (coll.tag == "HamsterInteractable"){
                if(Player.Onesie.type == OnesieType.Hamster){
                    InteractButton.SetActive(true);
                }
            }
        }
        
        private void OnTriggerExit(){
            InteractButton.SetActive(false);
        }


        public void UseOnesieSpecialAbility()
        {
            if (Onesie.type == OnesieType.Dragon)
            {
                StartCoroutine(SetEffectActiveForDuration("Flame Breath", 0.5f));
            }
        }

        public void RemoveCharge(int count)
        {
            Charges = Mathf.Max(Charges - count, 0);
        }

        public void AddCharges()
        {
            Charges = 3;
        }

        public bool HasOnesie(OnesieType type)
        {
            return PlayerPrefs.GetInt("Onesie_" + type.ToString(),0) == 1;
        }

        public void GiveOnesies()
        {
            if (onesies.ContainsKey("Dragon"))
                return;
            onesies.Add("Dragon", new DragonOnesie());
            onesies.Add("Elephant", new ElephantOnesie());
            onesies.Add("Hamster", new HamsterOnesie());
        }
    }
}