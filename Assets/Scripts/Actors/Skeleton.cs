﻿using UnityEngine;

namespace Nimbi.Actors {
	[RequireComponent(typeof(Animator))]
	public class Skeleton : MonoBehaviour {
		private Animator animator;
		private SkinnedMeshRenderer skeletonrenderer;

		public Animator Animator {
			get { return animator; }
		}

		public SkinnedMeshRenderer Renderer {
			get { return skeletonrenderer; }
		}

		public void Awake() {
			animator = GetComponent<Animator>();
            skeletonrenderer = GetComponentInChildren<SkinnedMeshRenderer>();
		}
	}
}