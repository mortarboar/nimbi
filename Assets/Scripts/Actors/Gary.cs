﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Actors
{
    [RequireComponent(typeof(Animator))]
    public class Gary : MonoBehaviour
    {
        float nextInteract;

        void Start()
        {
            float nextInteract = Time.time;
        }

        void Update()
        {
            if(nextInteract < Time.time)
            {
                GetComponent<Animator>().SetTrigger("Interact");
                nextInteract = Time.time + 10f;
            }
        }
    }
}
