﻿using UnityEngine;
using System.Collections;

public class CatCompanion : MonoBehaviour
{

    public GameObject leader;

    private Animator animator;
    private bool isIdle = false;
    private float idleTimer;
    private Vector3 targetIdlePosition;
    NavMeshHit hit;
    private NavMeshAgent agent;

    void Start()
    {
        animator = GetComponent<Animator>();
        idleTimer = Time.time;
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        float distanceFromLeader = Vector3.Distance(leader.transform.position, this.transform.position);

        if (distanceFromLeader > 3)
        {
            MoveToLeader();
        }
        else
        {
            idleTimer += Time.deltaTime;

            if (idleTimer > 5)
            {
                Idle();
            }
        }
    }

    private void MoveToLeader()
    {
        animator.SetBool("isWalking", true);
        agent.SetDestination(leader.transform.position);
        agent.angularSpeed = 300;
    }

    private void Idle()
    {
        if (!isIdle)
        {
            animator.SetBool("isWalking", true);
            isIdle = true;
            Vector3 randomDirection = Random.insideUnitSphere * 3f;
            randomDirection += leader.transform.position;
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection, out hit, 3f, 1);
            agent.SetDestination(hit.position);
            agent.angularSpeed = 300;
        }
        else if ((agent.pathStatus == NavMeshPathStatus.PathComplete || agent.pathStatus == NavMeshPathStatus.PathPartial) && agent.remainingDistance < 0.3f)
        {
            isIdle = false;
            idleTimer = 0;
            agent.angularSpeed = 0;
            animator.SetBool("isWalking", false);
        }
    }
}
