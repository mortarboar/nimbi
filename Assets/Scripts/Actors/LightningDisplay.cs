﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Actors
{
    public class LightningDisplay : MonoBehaviour
    {

        public GameObject[] lightning;
        public Player player;
        private int charges;

        void Start()
        {
            foreach (GameObject thunder in lightning)
            {
                thunder.GetComponent<Animator>().SetBool("Animating", true);
            }
        }

        void Update()
        {
            if (player.Charges != charges)
            {
                charges = player.Charges;
                for (int i = 0; i < lightning.Length; i++)
                {
                    if (i < player.Charges)
                        lightning[i].GetComponent<Renderer>().enabled = true;
                    else
                        lightning[i].GetComponent<Renderer>().enabled = false;
                }
            }
        }
    }
}