﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Actors
{
    public class OnesieHead : MonoBehaviour
    {
        public OnesieType type;

        void Update()
        {
            if (Player.Onesie.type == type)
                GetComponent<Renderer>().enabled = true;
            else
                GetComponent<Renderer>().enabled = false;
        }
    }
}