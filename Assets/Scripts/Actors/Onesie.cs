﻿using System;
using UnityEngine;

namespace Nimbi.Actors {
	public abstract class Onesie {

        abstract public bool canMoveObjects { get; }
		abstract public bool isHeavy { get; }
		abstract public float movementSpeed { get;}
		abstract public OnesieType type { get; }
	}

    public class DragonOnesie : Onesie
    {
        public override bool canMoveObjects { get { return false; } }

        public override bool isHeavy { get { return false; } }

        public override float movementSpeed { get { return 4.5f; } }

        public override OnesieType type { get { return OnesieType.Dragon; } }
    }

    public class ElephantOnesie : Onesie
    {
        public override bool canMoveObjects { get { return true; } }

        public override bool isHeavy { get { return true;}}

        public override float movementSpeed{get{ return 3; }}

        public override OnesieType type { get{ return OnesieType.Elephant; }}
    }

    public class HamsterOnesie : Onesie
    {
        public override bool canMoveObjects { get { return false; } }

        public override bool isHeavy { get { return false; } }

        public override float movementSpeed { get { return 6; } }

        public override OnesieType type { get { return OnesieType.Hamster; } }
    }

    public class HumanOnesie : Onesie
    {
        public override bool canMoveObjects { get { return false; } }

        public override bool isHeavy { get { return false; } }

        public override float movementSpeed { get { return 4.5f; } }

        public override OnesieType type { get { return OnesieType.Human; } }
    }

    public enum OnesieType {
		Human,
        Dragon,
		Elephant,
		Hamster
	}
}