﻿using UnityEngine;
using System.Collections;

public class Backpack : MonoBehaviour
{

    public Animator backpack, onesie;
    public ParticleSystem particles;

    void Start()
    {
        backpack.SetTrigger("Animate");
        StartCoroutine(Animate());
    }

    void Update()
    {
        transform.localRotation = Quaternion.Euler(330, 0, 0);
    }

    IEnumerator Animate()
    {
        yield return new WaitForSeconds(1.8f);
        onesie.GetComponent<Renderer>().enabled = true;
        onesie.SetTrigger("Animate");
        particles.Play();
    }

}