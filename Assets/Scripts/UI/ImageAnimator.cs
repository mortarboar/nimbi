﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Nimbi.UI
{
    public class ImageAnimator : MonoBehaviour
    {
        public Image[] images;
        public float fps;
        private float nextChange;
        private int currentImage;

        void Start()
        {
            foreach(Image im in images)
            {
                im.enabled = false;
            }
        }

        void Update()
        {
            if(nextChange < Time.time)
            {
                nextChange = Time.time + (1f / fps);
                images[currentImage].enabled = false;
                currentImage = (currentImage+1) % images.Length;
                images[currentImage].enabled = true;
            }
        }
    }
}