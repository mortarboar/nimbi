﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]

public class FadeToBlack : MonoBehaviour
{

    Image img;
    bool fading;
    bool goToBlack = true;

    public GameObject gary;

    private GameObject player;

    public Vector3 startPosition;
    public Vector3 garyPosition;

    void Awake()
    {
        img = GetComponent<Image>();
        img.color = new Color(0, 0, 0, 0);

    }

    // Update is called once per frame
    void Update()
    {

        if (fading)
        {
            Color c = img.color;
            if (goToBlack)
            {
                c.a += Time.deltaTime;
                if (c.a > 1)
                {
                    player.transform.position = startPosition;
                    gary.transform.localPosition = garyPosition;
                    gary.transform.localRotation = Quaternion.Euler(0, 294.3972f, 0);
                    goToBlack = false;
                }
            }

            else
                c.a -= Time.deltaTime;
            img.color = c;
            if (c.a < 0)
                Destroy(gameObject);
        }
    }



    public void BeginFade(GameObject player)
    {
        fading = true;
        this.player = player;
    }
}
