﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Nimbi.UI
{
    [RequireComponent(typeof(Text))]
    public static class DialogueManager
    {
        /// <summary>
        /// FIFO queue system to display dialogues.
        /// </summary>
        private static List<DialogueText> dialogueQueue = new List<DialogueText>();

        private static DialogueDisplay display;

        /// <summary>
        /// Adds a Dialogue to the Dialogue Queue
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="duration">Duration the text should be shown</param>
        public static void Add(string text, float duration)
        {
            Add(text, duration, new Color32(255,255,255,255));
        }

        /// <summary>
        /// Adds a Dialogue to the Dialogue Queue with a nice color
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="duration">Duration the text should be shown</param>
        /// <param name="color">Color of the text</param>
        public static void Add(string text, float duration, Color color)
        {
            dialogueQueue.Add(new DialogueText(text, duration, color));
        }

        public static void Remove(string text)
        {
            foreach(DialogueText txt in dialogueQueue)
            {
                if (txt.Text == text)
                {
                    dialogueQueue.Remove(txt);
                    break;
                }
            }
            display.Remove(text);
        }

        public static void AddDisplay(DialogueDisplay display)
        {
            DialogueManager.display = display;
        }

        public static void Clear()
        {
            dialogueQueue = new List<DialogueText>();
            display.Clear();
        }

        public static List<DialogueText> GetQueue()
        {
            return dialogueQueue;
        }
    }
}