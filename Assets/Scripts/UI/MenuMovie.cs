﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(RawImage))]
public class MenuMovie : MonoBehaviour {

    private MovieTexture movie;
    private bool movieStarted;
    private bool movieFinished;

	void Start () {
        movie = GetComponent<RawImage>().texture as MovieTexture;
	}
	

	void Update () {
        if(!movieStarted)
        {
            GetComponent<RawImage>().enabled = true;
            movie.Play();
            movieStarted = true;
        }
        else if(movieStarted && !movie.isPlaying)
        {
            GetComponent<RawImage>().enabled = false;
            Color c = Camera.main.backgroundColor;
            c = Vector4.MoveTowards(c, Color.black, Time.deltaTime * 0.8f);
            Camera.main.backgroundColor = c;
        }
	}
}
