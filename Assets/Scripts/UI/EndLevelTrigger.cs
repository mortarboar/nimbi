﻿using UnityEngine;
using Nimbi.Framework;
using Nimbi;
using UnityEngine.SceneManagement;

public class EndLevelTrigger : MonoBehaviour
{

    private bool inTrigger;

    public EndBlackFader fader;


    // Update is called once per frame
    void Update()
    {
        if (inTrigger)
        {
            fader.BeginFade();
        }

    }

    public void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player" && !inTrigger)
        {
            inTrigger = true;
            UnityAnalytics.CompleteLevel(SceneManager.GetActiveScene().name, Mathf.RoundToInt(Time.time - GameManager.Instance.levelStart));
        }
    }
}
