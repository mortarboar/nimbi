﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Nimbi.Framework;

namespace Nimbi.UI
{
    public class Dialogue : MonoBehaviour
    {
        public float showForSeconds = 1f;
        public string dialogueText;
        public TypeCollider type = TypeCollider.Player;
        public StartOnTrigger onTrigger = StartOnTrigger.Enter;

        void OnTriggerEnter(Collider col)
        {
            if (onTrigger == StartOnTrigger.Enter)
            {
                showText(col);
            }
        }

        void OnTriggerExit(Collider col)
        {
            if (onTrigger == StartOnTrigger.Exit)
            {
                showText(col);
            }
        }

        private void showText(Collider col)
        {
            if (type == TypeCollider.Elephant)
            {
                if (col && col.name == "Elephant")
                {
                    DialogueManager.Add(dialogueText.Localize(), showForSeconds);
                    Destroy(this);
                }
            }
            else if (type == TypeCollider.Player)
            {
                if (col && col.tag == "Player")
                {
                    DialogueManager.Add(dialogueText.Localize(), showForSeconds);
                    Destroy(this);
                }
            }
        }

    }
}


