﻿using Nimbi.Actors;
using Nimbi.Framework;
using Nimbi.Interaction;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

namespace Nimbi.UI {
    public sealed class HUD : MonoBehaviour {
        private LevelSlider levelSlider;
        private WaterMazeManager mazeManager;

        public GameObject buoyCounter;
        public Text buoyCounterLeftDigit, buoyCounterRightDigit;

        public Image elephantSlot ;

        public Image Onesietip;
        public DepthOfField depth;



        public void Awake() {
            this.buoyCounter.SetActive(false);
            this.mazeManager = FindObjectOfType<WaterMazeManager>();
            Invalidate();
        }

        private void Invalidate() {
            // Main HUD elements
      

            // Buoy HUD elements
            if (mazeManager == null) {
                buoyCounter.SetActive(false);
            } else {
                if (levelSlider == null) {
                    levelSlider = GameObject.Find("LevelSlider/Segment1").GetComponent<LevelSlider>();
                }

                string digits = string.Format("{0:00}", 12 - mazeManager.GatesOpened);

                buoyCounter.SetActive(levelSlider.InTrigger);
                buoyCounterLeftDigit.text = digits[0].ToString();
                buoyCounterRightDigit.text = digits[1].ToString();
            }
        }

        public void Update() {
            Invalidate();
        }

        public void ShowOnesieTip(OnesieType type)
        {
            Onesietip.enabled = true;
            depth.enabled = true;
        }

        public void HideOnesieTip()
        {
            Onesietip.enabled = false;
            depth.enabled = false;
        }
    }
}