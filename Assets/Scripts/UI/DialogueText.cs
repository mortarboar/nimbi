﻿using UnityEngine;
using System.Collections;

namespace Nimbi.UI
{
    public class DialogueText
    {
        public string Text;
        public float Duration;
        public Color Color;

        public DialogueText(string text, float duration, Color color)
        {
            this.Text = text;
            this.Duration = duration;
            this.Color = color;
        }
    }
}