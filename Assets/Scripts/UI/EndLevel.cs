﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]

public class EndLevel : MonoBehaviour
{

    Image img;
    bool fading;
    bool goToBlack = true;

   
    void Awake()
    {
        img = GetComponent<Image>();
        img.color = new Color(0, 0, 0, 0);

    }

    // Update is called once per frame
    void Update()
    {

        if (fading)
        {
            Color c = img.color;
            if (goToBlack)
            {
                c.a += Time.deltaTime;
              
            }

            else
                c.a -= Time.deltaTime;
            img.color = c;
            if (c.a < 0)
                Destroy(gameObject);
        }
    }



    public void BeginFade(GameObject player)
    {
        fading = true;
    }
}
