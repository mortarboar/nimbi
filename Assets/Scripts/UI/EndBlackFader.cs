﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(Image))]
public class EndBlackFader : MonoBehaviour
{

    Image img;
    bool fading;
    float levelstart;
    public int loadLevel;
    private bool levelLoaded;


    void Awake()
    {
        img = GetComponent<Image>();
        img.color = new Color(0, 0, 0, 0);
        levelstart = Time.time;

    }

    void Update()
    {
        if (fading)
        {
            Color c = img.color;
            c.a += ((Time.time + levelstart) * 0.005f);
            img.color = c;
            if (c.a >= 1 && !levelLoaded)
            {
                levelLoaded = true;
                SceneManager.LoadSceneAsync(loadLevel);
            }
        }

    }




    public void BeginFade()
    {
        fading = true;

    }
}


