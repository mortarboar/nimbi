﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Nimbi.UI
{
    [RequireComponent(typeof(Text))]
    public class DialogueDisplay : MonoBehaviour
    {
        private float dialogueStart;
        
        public GameObject chatBox;
        private Text text;

        void Start()
        {
            text = GetComponent<Text>();
            text.text = "";
            DialogueManager.AddDisplay(this);
        }

        void Update()
        {
            if (DialogueManager.GetQueue().Count > 0)
            {
                if (text.text == "")
                {
                    chatBox.SetActive(true);
                    text.text = DialogueManager.GetQueue()[0].Text;
                    text.color = DialogueManager.GetQueue()[0].Color;
                    dialogueStart = Time.time;
                }
                else if (dialogueStart + DialogueManager.GetQueue()[0].Duration < Time.time)
                {
                    chatBox.SetActive(false);
                    DialogueManager.Remove(text.text);
                    text.text = "";
                }
            }
        }

        public void Clear()
        {
            text.text = "";
            dialogueStart = 0;
        }

        public bool Remove(string text)
        {
            if(this.text.text == text)
            {
                this.text.text = "";
                dialogueStart = 0;
                return true;
            }
            return false;
        }
    }
}