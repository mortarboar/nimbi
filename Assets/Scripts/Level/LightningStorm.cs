﻿using UnityEngine;
using System.Collections;


public class LightningStorm : MonoBehaviour {
	
	public Light lightning;
	

	void Start()
	{
		lightning = GetComponent<Light>();
	}
	
	void Update()
	{
		StartCoroutine("ThunderSpawn", Random.Range(0.5f, 3.0f));
	}
	
	IEnumerator ThunderSpawn()
	{
		lightning.enabled = true;	
	   yield return new WaitForSeconds(6);
	   lightning.enabled = false;
	}
	
}
