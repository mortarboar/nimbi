﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction {
	public class WheelPimScript : MonoBehaviour {


		public Player player;
		public Transform stepInPosition;
		
		public Transform stepOutPosition;
		
		public float waterHeight = 0.315f;
		private float newHeight = 0f;
		private Vector3 defaultPosition;
		private bool pumpingLeft;
		
		private bool pumpingRight;
		
		public GameObject garyWater;
		

		public GameObject wheel;

		
	
		
		public bool canClimbIn;
		public bool insideWheel;
		

		void Start()
		{
			
			
		}

		void Update()
		{
			
			//When we press a button to enter our HamsterWheel
			if(Input.GetButtonDown("Submit")){
				EnterAndLeaveWheel();	
			}
			
			if(insideWheel){
				DisableMovementInWheel();
			}
			
						
			if (pumpingLeft){
				wheel.transform.Rotate(new Vector3(2f,0,0));
				garyWater.SetActive(true);	
				
			}
			 if (pumpingRight){
				wheel.transform.Rotate(new Vector3(-2f,0,0));
				garyWater.SetActive(false);
			}
			
			
							
			
			//When Pumping Raise the Water and Higher our Little Nut floating on it!
			

		}

		void OnTriggerEnter(Collider coll)
		{
			if (coll.tag == "Player")
			{
				canClimbIn = true;
			}
		}


		
		
		void EnterAndLeaveWheel(){
			if(!insideWheel && canClimbIn && Player.Onesie.type == OnesieType.Hamster)
            {
				player.transform.position = stepInPosition.transform.position;
				canClimbIn = false;
                insideWheel = true;
			}
            else if (insideWheel)
            {
                player.transform.position = stepOutPosition.transform.position;
                insideWheel = false;
                canClimbIn = true;
            }

		}			

		void OnTriggerExit(Collider coll){
            if(coll.tag == "Player")
                canClimbIn = false;
		}
		
		
		void DisableMovementInWheel(){
		//Disable Movement While in HamsterWheel and check what Axis are used!
				if(insideWheel && Input.GetAxisRaw("Horizontal") > 0)
				{
					pumpingLeft= false;
					pumpingRight = true;
				}	
				else if(insideWheel && Input.GetAxisRaw("Horizontal") < 0)
				{
					pumpingRight=false;
					pumpingLeft = true;
				}
				else if(insideWheel){
					pumpingRight=false;
					pumpingLeft = false;
				}
						
}

}

}

