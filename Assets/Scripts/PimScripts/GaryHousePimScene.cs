﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;
using Nimbi.UI;

namespace Nimbi.Interaction.Triggers
{
    public class GaryHousePimScene : MonoBehaviour
    {
        public GameObject stairs, mainCamera, cutsceneCamera;
        public GameObject targetPlate, targetStairs;
        public GameObject groundParticle;


        public AudioClip earthQuake;

        private bool playerInRange = false;
        private bool plateActivated = false;
        private bool cutsceneActive = false;
        private bool cutsceneSkipped = false;
        private bool textShown;
        private bool stairsComplete;

        void Start()
        {
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        }

        void Update()
        {
            if (playerInRange)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPlate.transform.position, Time.deltaTime / 4);

                if (transform.position == targetPlate.transform.position && !plateActivated)
                {
                    plateActivated = true;
                    AudioSource.PlayClipAtPoint(earthQuake, this.transform.position);
                }
            }

            if (plateActivated && !stairsComplete)
            {
                stairs.transform.position = Vector3.MoveTowards(stairs.transform.position, targetStairs.transform.position, Time.deltaTime);
                
                Debug.Log("Rad is op " + stairs.transform.position);
                Debug.Log("Stairs is Op" + targetStairs.transform.position);

                if (stairs.transform.position == targetStairs.transform.position)
                {
                    StartCoroutine("DelayCameraSwitch");
                    if (!textShown)
                    {
                        DialogueManager.Add("gary.award_winning_flowers".Localize(), 2f);
                        textShown = true;
                        stairsComplete = true;
                    }

                }

                if (cutsceneActive && !cutsceneSkipped)
                {
                    groundParticle.GetComponent<ParticleSystem>().Play();
                    cutsceneCamera.transform.position = Vector3.MoveTowards(cutsceneCamera.transform.position, GameObject.Find("cutsceneCameraTarget").transform.position, Time.deltaTime / 20);

                    if (Input.GetButtonDown("Submit"))
                    {
                        groundParticle.GetComponent<ParticleSystem>().Stop();
                        mainCamera.SetActive(true);
                        cutsceneCamera.SetActive(false);
                        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().EnableMovement();
                        cutsceneActive = false;
                        cutsceneSkipped = true;
                    }
                }
            }
        }

        private void OnTriggerStay(Collider coll)
        {
            if (coll.tag == "Player" && Player.Onesie.type == OnesieType.Elephant)
                playerInRange = true;

            if (plateActivated && !cutsceneSkipped)
            {
                cutsceneActive = true;
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().DisableMovement();
                cutsceneCamera.SetActive(true);
                mainCamera.SetActive(false);
            }
        }

        IEnumerator DelayCameraSwitch()
        {
            cutsceneActive = false;
            cutsceneSkipped = true;
            groundParticle.GetComponent<ParticleSystem>().Stop();
            yield return new WaitForSeconds(2);

            mainCamera.SetActive(true);
            cutsceneCamera.SetActive(false);
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().EnableMovement();
            Destroy(this);
        }
    }
}