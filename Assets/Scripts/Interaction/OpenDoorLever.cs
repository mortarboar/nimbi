﻿using UnityEngine;
using System.Collections;

using Nimbi.Framework;

namespace Nimbi.Interaction
{

    public class OpenDoorLever : MonoBehaviour
    {
        public GameObject targetDoor;
        public GameObject switchLamp;

        private bool inTrigger;
        private bool doorOpen = false;

        void Start()
        {
            Material m = switchLamp.GetComponent<Renderer>().material;
            m.color = Color.red;
            switchLamp.GetComponent<Renderer>().material = m;
        }

        void Update()
        {
            if (inTrigger && Input.GetButtonDown("Submit"))
            {
                if(doorOpen)
                {
                    CloseDoor();
                    GetComponent<Animator>().SetBool("GoingDown", false);
                    Material m = switchLamp.GetComponent<Renderer>().material;
                    m.color = Color.red;
                    switchLamp.GetComponent<Renderer>().material = m;
                }
                else
                {
                    OpenDoor();
                    GetComponent<Animator>().SetBool("GoingDown", true);
                    Material m = switchLamp.GetComponent<Renderer>().material;
                    m.color = Color.green;
                    switchLamp.GetComponent<Renderer>().material = m;
                }
            }
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.gameObject.tag == "Player")
            {
                inTrigger = true;
            }
        }
        void OnTriggerExit(Collider coll)
        {
            if (coll.gameObject.tag == "Player")
            {
                inTrigger = false;
            }
        }

        // Open the door to the given position.
        private void OpenDoor()
        {
            targetDoor.transform.Rotate(new Vector3(0, 90, 0)); // Rotate, so it `opens`
            doorOpen = true;
        }

        private void CloseDoor()
        {
            targetDoor.transform.Rotate(new Vector3(0, -90, 0)); // Rotate, so it `closes`
            doorOpen = false;
        }


    }
}