﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;
using Nimbi.UI;
using UnityEngine.UI;
using Nimbi.CameraControl;

namespace Nimbi.Interaction
{
    public class Wipwap : MonoBehaviour
    {

        public Player player;
        public GameObject wipwap;
        public Trashbag trash;
        private bool inTrigger;
        private bool activated;
        private Quaternion goalRotation;
        private bool dialogueShown;
        public Image elephantImage;
        public Onesie elephantOnesie;
        public HUD HUD;
        public GameObject ElephantOnesieBackpack;
        private GameObject backpackDisplay;
        private bool onesiePopupHidden;
        public bool completed;

        void Start()
        {
            goalRotation = Quaternion.Euler(63.5f, 81, 350);
        }

        void Update()
        {
            // Player is in the trigger and is an elephant, so move that stuff
            if (inTrigger && Player.Onesie.type == OnesieType.Elephant && !activated)
            {
                activated = true;
                trash.MoveUp();
            }
            if (inTrigger && Player.Onesie.type != OnesieType.Elephant && !activated && !dialogueShown)
            {
                dialogueShown = true;
                DialogueManager.Add("gary.give_suit".Localize(), 2f);

                // Geef onesie
                elephantImage.enabled = true;
                player.AddOnesie(OnesieType.Elephant);
                HUD.ShowOnesieTip(OnesieType.Elephant);
                backpackDisplay = Instantiate(ElephantOnesieBackpack, Vector3.zero, Quaternion.identity) as GameObject;
                backpackDisplay.transform.parent = Camera.main.transform;
                backpackDisplay.transform.localPosition = new Vector3(0, 0, 8);
                Camera.main.GetComponent<MainCameraScript>().degree = -1;
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -15);
                player.GetComponent<PlayerController>().DisableMovement();
                DialogueManager.Add("Press Interact to close", 5);
            }
            if (dialogueShown && !onesiePopupHidden && Input.GetButtonDown("Submit"))
            {
                onesiePopupHidden = true;
                DialogueManager.Remove("Press Interact to close");
                player.GetComponent<PlayerController>().EnableMovement();
                Destroy(backpackDisplay);
                HUD.HideOnesieTip();
                Camera.main.transform.localPosition = new Vector3(0, 0, -15);
            }

            if (activated)
            {
                if (wipwap.transform.localRotation == Quaternion.RotateTowards(wipwap.transform.localRotation, goalRotation, 8.0f))
                {
                    activated = false;
                    completed = true;
                }
                wipwap.transform.localRotation = Quaternion.RotateTowards(wipwap.transform.localRotation, goalRotation, 8.0f);
            }
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = true;
        }

        void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = false;
        }
    }
}