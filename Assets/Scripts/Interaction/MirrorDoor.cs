﻿using UnityEngine;
using System.Collections;

public class MirrorDoor : MonoBehaviour {

    private bool inTrigger = false;
    private bool opening;
    private Vector3 goalRotation;

    void Update()
    {
        if(inTrigger && Input.GetButton("Submit") && !opening)
        {
            opening = true;
            goalRotation = transform.rotation.eulerAngles + new Vector3(0, -90, 0);
        }

        if(opening)
        {
            Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(goalRotation), 60);
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider coll)
    {
        inTrigger = false;
    }
}
