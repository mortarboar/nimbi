﻿using Nimbi.Platform;
using UnityEngine;
using Nimbi.Audio;

namespace Nimbi.Interaction
{
    public class Door : MonoBehaviour
    {
        public GameObject originDoor;
        public GameObject mirrorPlayer;

        public GameObject key;
        private bool openingDoor = false;
        private Vector3 ownGoal, originalGoal;

        private void Update()
        {
            if (openingDoor)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(ownGoal), 60);
                originDoor.transform.rotation = Quaternion.RotateTowards(originDoor.transform.rotation, Quaternion.Euler(originalGoal), 90*Time.deltaTime);
            }
        }

        private void OnTriggerStay(Collider coll)
        {
            if (coll.tag == "Player")
            {
                if (Input.GetButtonDown("Submit"))
                {
                    if (key != null && key.transform.parent == mirrorPlayer.transform)
                    {
                        OpenDoor();
                    }
                }
            }
        }

        private void OpenDoor()
        {
            AudioManager.Instance.PlaySoundeffect(AudioManager.Instance.doorOpenSoundFile);
            Destroy(key);
            ownGoal = transform.rotation.eulerAngles + new Vector3(0, -90, 0);
            originalGoal = originDoor.transform.rotation.eulerAngles + new Vector3(0, -90, 0);
            openingDoor = true;
        }
    }
}
