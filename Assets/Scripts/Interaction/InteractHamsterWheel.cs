﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction {
	public class InteractHamsterWheel : MonoBehaviour {


		public Player player;
		public Transform stepInPosition;
		
		public Transform stepOutPosition;
		
		public GameObject waterToPump;
		public GameObject newWater;
		public float waterHeight = 0.315f;
		private float newHeight = 0f;
		private Vector3 defaultPosition;
		private bool pumping;
		public float pumpDelay = 1.0f;
		private float nextPump;
        private int waterAmount = 0;
		public GameObject wheel;
		private GameObject nut;
        private Vector3 nutgoal;
		
	
		
		public bool canClimbIn;
		public bool insideWheel;

		void Start()
		{
			nextPump = Time.time;
			defaultPosition = newWater.transform.localPosition;
            nut = GameObject.Find("Nut");
            nutgoal = nut.transform.position;
			
		}

		void Update()
		{
			
			//When we press a button to enter our HamsterWheel
			if(Input.GetButtonDown("Submit")){
				EnterAndLeaveWheel();	
			}
			
			if(insideWheel){

				DisableMovementInWheel();
			}
			
						
			if (pumping){
				wheel.transform.Rotate(new Vector3(5f,0,0));
			}
			
							
			
			//When Pumping Raise the Water and Higher our Little Nut floating on it!
			if (nextPump < Time.time && pumping && waterToPump.GetComponent<HamsterWater>().spitcount > 0)
			{
				newHeight += waterHeight;
				nextPump = Time.time + pumpDelay;
				waterToPump.GetComponent<HamsterWater>().spitcount -= 1;
                waterAmount++;
                nutgoal += Vector3.up*0.6f;
                
			}

            if(nut != null && !nut.GetComponent<Nut>().pickedUp)
                nut.transform.position = Vector3.MoveTowards(nut.transform.position, nutgoal, Time.smoothDeltaTime * 1.5f);

            newWater.transform.localScale = Vector3.MoveTowards(newWater.transform.localScale, new Vector3(newWater.transform.localScale.x, newHeight, newWater.transform.localScale.z), Time.deltaTime * 1.5f);
			newWater.transform.localPosition = Vector3.MoveTowards(newWater.transform.localPosition, new Vector3(newWater.transform.localPosition.x, defaultPosition.y + newHeight, newWater.transform.localPosition.z), Time.deltaTime * 1.5f);
			

			}

		void OnTriggerStay(Collider coll)
		{
			if (coll.tag == "Player" && Player.Onesie.type == OnesieType.Hamster)
			{
				Debug.Log("Nimbi can Step in");
				canClimbIn = true;
			}
		}

		void OnTriggerExit(Collider coll)
		{
			canClimbIn = false;
		}
		
		
		void EnterAndLeaveWheel(){
			if(!insideWheel && canClimbIn){
				player.transform.position = stepInPosition.transform.position;
				insideWheel = true;
			}
			else if(insideWheel) {
				Debug.Log("Going out");
				player.transform.position = stepOutPosition.transform.position;
				insideWheel = false;
				player.GetComponent<PlayerController>().EnableMovement();
			}
			
			
		}
		
		
		void DisableMovementInWheel(){
		//Disable Movement While in HamsterWheel and check what Axis are used!
			player.GetComponent<PlayerController>().DisableMovement();
				if(Input.GetAxis("Horizontal") >= 0)
				{
					pumping = true;
				}	
				if(Input.GetAxis("Horizontal") < -0)
				{
					pumping = false;
				}
				
				
}

}

}

