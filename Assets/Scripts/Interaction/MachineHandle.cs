﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Interaction
{
    public class MachineHandle : MonoBehaviour
    {

        public Electropaal[] poles;
        public GameObject stairs;
        public GameObject chargeLight;
        bool inTrigger;
        Vector3 goalRotation;
        bool rotating;
        bool canBeRotated;
        int activatedPoles = 0;

        void Start()
        {
            goalRotation = transform.localRotation.eulerAngles - new Vector3(90, 0, 0);
            stairs.transform.localRotation = Quaternion.Euler(0, 0, 300);
        }

        void Update()
        {
            if (canBeRotated && !rotating && Input.GetButtonDown("Submit") && inTrigger)
            {
                rotating = true;
            }

            if (rotating)
            {
                transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(goalRotation), 90 * Time.deltaTime);
                stairs.transform.localRotation = Quaternion.RotateTowards(stairs.transform.localRotation, Quaternion.Euler(Vector3.zero), 60 * Time.deltaTime);
            }
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = true;
        }

        void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = false;
        }

        public void ActivatePole()
        {
            activatedPoles++;
            if (activatedPoles >= 3)
            {
                canBeRotated = true;
                chargeLight.SetActive(true);
            }
        }
    }
}