﻿using UnityEngine;
using System.Collections;
using Nimbi.UI;

namespace Nimbi.Interaction
{
    public class Trashbag : MonoBehaviour
    {

        private bool movingUp;
        public Gardendoor door;
       
        public AudioClip doorCrash;

        public Transform firstspot;

        // Update is called once per frame
        void Update()
        {
            if (movingUp)
            {
                transform.position = Vector3.MoveTowards(transform.position, firstspot.position, Time.deltaTime * 8f);
                if (transform.position == Vector3.MoveTowards(transform.position, firstspot.position, Time.deltaTime * 8f))
                {
                    movingUp = false;
                    AudioSource.PlayClipAtPoint(doorCrash, door.transform.position);
                        foreach(Transform child in this.transform){
                            child.GetComponent<MeshCollider>().enabled = false;
                            child.transform.parent = null;
                            }
                    door.Open();
   
                    this.GetComponent<MeshCollider>().enabled = false;
                    Rigidbody rb = gameObject.AddComponent<Rigidbody>();
                    //Remove Our Falling Objects from the Toolbox
                    
 
                    rb.AddForce(new Vector3(-30, 10, 0), ForceMode.Impulse);
                    DialogueManager.Add("gary.too_heavy".Localize(), 2f);
                }
            }
        }

        public void MoveUp()
        {
            movingUp = true;
        }

        public void Shoot()
        {
            movingUp = false;
        }
    }
}