﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Interaction
{

    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(Animator))]
    public class Grass : MonoBehaviour
    {

        void OnTriggerEnter()
        {
            GetComponent<Animator>().SetTrigger("InTrigger");
        }

        void Reset()
        {
            GetComponent<Collider>().isTrigger = true;
        }
    }
}