﻿using UnityEngine;
using System.Collections;

public class InteractionManager : MonoBehaviour
{

    public Animator interactableObject;
    bool inTrigger;

    void Update()
    {
        //If Player is in trigger.
        if(inTrigger && interactableObject.HasParameterOfType("Colliding",AnimatorControllerParameterType.Trigger)){
            interactableObject.SetTrigger("Colliding");
        }
        
        if (inTrigger && Input.GetButtonDown("Submit"))
            interactableObject.SetTrigger("Interact");
    }

    void OnTriggerEnter()
    {
        inTrigger = true;
    }

    void OnTriggerExit()
    {
        inTrigger = false;
    }
}
