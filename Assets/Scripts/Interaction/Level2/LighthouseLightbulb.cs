﻿using UnityEngine;
using System.Collections;

public class LighthouseLightbulb : MonoBehaviour
{

    private GameObject player;
    private bool pickedUp = false;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerStay(Collider coll)
    {
        if (!pickedUp) {
            if (Input.GetButtonDown("Submit") && coll.tag == "Player") {
                pickedUp = true;
                this.transform.parent = player.transform;
                this.transform.localPosition = new Vector3(0, 2f, 0);
                this.transform.localScale = new Vector3(.5f, .5f, .5f);
            }
        }
    }
}
