﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;


namespace Nimbi.Interaction
{
    public class PickableHandler : MonoBehaviour
    {
        public Player player;
        public bool holdingItem;

        public void PickUp(GameObject item)
        {
                    item.transform.SetParent(player.transform);
                    item.transform.localPosition = new Vector3(0, 0.91f, 0.46f);
        }
    }
}

