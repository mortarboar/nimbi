﻿using Nimbi.Actors;
using UnityEngine;

namespace Nimbi.Interaction
{
    public class WaterInteraction : MonoBehaviour
    {

        void Start()
        {

        }

        private void OnTriggerStay(Collider coll)
        {
            if (!Player.Onesie.isHeavy)
                coll.GetComponent<Rigidbody>().AddForce(Vector3.up, ForceMode.Impulse);
        }
    }
}