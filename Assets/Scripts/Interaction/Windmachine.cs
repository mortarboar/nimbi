﻿using Nimbi.Framework;
using Nimbi.Actors;
using Nimbi.Interaction.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Nimbi.Objects
{
    [RequireComponent(typeof(BoxCollider))]
    public class Windmachine : MonoBehaviour
    {
        private State currentState = State.Inactive;
        private bool inTrigger;
        private Player player;
        public WindTrigger wind;

        public GameObject[] doors;
        public GameObject batteryLight;
        public GameObject batteryImage;
        public Sprite emptyBattery;
        public Sprite fullBattery;

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        private void FixedUpdate()
        {
            //If the player is statically charged and is inside the trigger activating the machine
            if (inTrigger && player.Charges > 0)
            {
                foreach (GameObject door in doors)
                {
                    if (door.GetComponent<MeshCollider>() != null)
                    {
                        door.GetComponent<Rigidbody>().isKinematic = false;
                        Destroy(door.GetComponent<MeshCollider>());
                    }
                }

                //Then activate the machine + wind
                ChangeState(State.Active);

                //Remove charges from the player
                player.RemoveCharge(3);

                //Set the new battery image
                batteryImage.GetComponent<Image>().sprite = fullBattery;

                //Set the new battery light color
                batteryLight.GetComponent<Light>().color = Color.green;
            }

            //Set state from trigger to state of machine
            wind.state = currentState;
        }

        private void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            {
                inTrigger = true;
            }
        }

        private void OnTriggerExit()
        {
            inTrigger = false;
        }

        private void ChangeState(State newState)
        {
            if (newState != currentState)
            {
                currentState = newState;
            }
        }
    }
}