﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction
{
    [RequireComponent(typeof(BoxCollider))]
    public class Electropaal : MonoBehaviour
    {

        public bool charged;
        public Color chargedColor;
        public GameObject chargedLight;
        public MachineHandle handle;

        void OnTriggerEnter(Collider coll)
        {
            if (!charged && coll.tag == "Player" && coll.GetComponent<Player>().Charges > 0)
            {
                charged = true;
                coll.GetComponent<Player>().RemoveCharge(1);
                Material m = GetComponent<Renderer>().materials[2];
                m.color = chargedColor;
                GetComponent<Renderer>().materials[2] = m;
                chargedLight.SetActive(true);
                if (handle != null)
                    handle.ActivatePole();
            }
        }
    }
}