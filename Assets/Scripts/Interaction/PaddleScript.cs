﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction {

public class PaddleScript : MonoBehaviour {


private bool inTrigger;
private Collider paddleCol;

public FadeToBlack fader;

public bool hasPaddle;


public void Start()
{
    
}

public void Update(){
if(inTrigger && Input.GetButtonDown("Submit") && !hasPaddle)
{
            this.transform.parent = paddleCol.transform;
            this.transform.localPosition = new Vector3(0, 2.5f);
            hasPaddle = true;
            fader.BeginFade(paddleCol.gameObject);
  }
}

public void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            paddleCol = coll;
            inTrigger = true;
        }
    }
}
