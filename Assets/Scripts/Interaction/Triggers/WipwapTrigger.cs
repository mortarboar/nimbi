﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction.Triggers
{
    [RequireComponent(typeof(BoxCollider))]
    public class WipwapTrigger : MonoBehaviour
    {
        public enum Direction { Left,Right};

        public Direction direction;
        public GameObject wipwap;
        private Vector3 newRotation = new Vector3(63.5f, 81, 350);
        private bool inTrigger;
        public Wipwap wipwapTrigger;

        void Update()
        {
            if (inTrigger && wipwapTrigger.completed)
            {
                wipwap.transform.localRotation = Quaternion.RotateTowards(wipwap.transform.localRotation, Quaternion.Euler(newRotation), 90.0f*Time.deltaTime);
                Debug.Log("Rotating "+direction);
            }
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            {
                inTrigger = true;
                newRotation = (direction == Direction.Left ? new Vector3(63.5f, 81, 350) : new Vector3(64.4548f, 279.2493f, 186.491f));
            }
        }

        void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = false;
        }
    }
}