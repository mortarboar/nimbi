﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction.Triggers
{
    [RequireComponent(typeof(BoxCollider))]
    public class ChargeRemover : MonoBehaviour
    {

        void OnTriggerEnter(Collider coll)
        {
            if(coll.tag == "Player")
            {
                coll.GetComponent<Player>().RemoveCharge(3);
            }
        }
    }
}