﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;

namespace Nimbi.Interaction.Triggers
{
    public class PaddleTrigger : MonoBehaviour
    {
        public PaddleScript paddlescript;
        public GameObject paddle;
        public Vector3 paddlePosition;
        public Vector3 paddleRotation;
        public GameObject raft;
        public GameObject gary;
        public GameObject raftwall;

        private bool raftRotating;
        private bool inTrigger;
        private bool paddlePlaced = false;
        private Vector3 startRotation;

        // Update is called once per frame
        void Update()
        {
            if (inTrigger && Input.GetButtonDown("Submit") && !paddlePlaced)
            {
                if (paddlescript.hasPaddle)
                {
                    
                    Debug.LogWarning("paddle is in handjes");
                    
                    GameObject.FindGameObjectWithTag("Player").transform.parent = raft.transform;
                    gary.transform.parent = raft.transform;
                    gary.GetComponent<Animator>().SetTrigger("OnRaft");
                    paddle.transform.parent = raft.transform;
                    paddle.transform.localPosition = paddlePosition;
                    paddle.transform.localRotation = Quaternion.Euler(paddleRotation);

                    paddlePlaced = true;
                    raftRotating = true;
                    startRotation = raft.transform.localEulerAngles;
                    raftwall.SetActive(true);
                }
                else {
                    Debug.LogWarning("You can't do this yet!");
                }
            }

            if (paddlePlaced && raftRotating && paddlescript.hasPaddle)
            {
                raft.transform.localRotation = Quaternion.RotateTowards(raft.transform.localRotation, Quaternion.Euler(startRotation + new Vector3(0, 180, 0)), 60*Time.deltaTime);
                if(raft.transform.localRotation == Quaternion.RotateTowards(raft.transform.localRotation, Quaternion.Euler(startRotation + new Vector3(0, 180, 0)), 60*Time.deltaTime))
                {
                    raftRotating = false;
                }
            }
            else if(paddlePlaced)
                raft.transform.Translate(Vector3.forward * (1f * Time.deltaTime));

        }

        public void OnTriggerEnter(Collider coll)
        {
            if (coll.attachedRigidbody)
            {
                inTrigger = true;
                Debug.Log("nimbi is in trigger");
            }
        }
        
        public void OnTriggerExit(){
            inTrigger = false;
        }

    }

}
