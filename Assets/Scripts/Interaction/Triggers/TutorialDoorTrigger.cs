﻿using UnityEngine;
using System.Collections;
using System;
using Nimbi.UI;

namespace Nimbi.Interaction.Triggers
{
    public class TutorialDoorTrigger : MonoBehaviour
    {
        public AudioClip doorBell;
        public AudioClip doorSound;
              
        public GameObject bell;
        public Transform gate;
        public bool doorBellPickedUp = false;
        
        public string watchYourself;

        public AudioClip puzzleComplete;
        public float degreesPerSecond, rotationDegreesAmount;

        private bool doorUnlocked = false;
        private float totalRotation;

        private bool triedToOpen;

        public string missingBell;
        
        public string trash;

        public float dialogueLength;
        

        private void Update()
        {
            if (doorUnlocked)
            {
                if (Mathf.Abs(totalRotation) < Mathf.Abs(rotationDegreesAmount))
                    OpenDoor();
                else
                {
                    Destroy(this);
                }
            }
        }

        private void OpenDoor()
        {
            float currentAngle = gate.rotation.eulerAngles.y;
            gate.rotation = Quaternion.AngleAxis(currentAngle + (Time.deltaTime * degreesPerSecond), Vector3.up);
            totalRotation += Time.deltaTime * degreesPerSecond;
        }
        
        
        private void OnTriggerEnter(Collider coll){
             if(coll.tag == "Trash"){
                DialogueManager.Add(trash.Localize(), dialogueLength);
            }
        }
        
        
        
        private void OnTriggerStay(Collider coll)
        {
            if (coll.tag == "Player" && Input.GetButtonDown("Submit"))
            {
                if (doorBellPickedUp)
                {
                    doorUnlocked = true;
                    bell.transform.parent = GameObject.Find("FirstPlatform").transform;
                    bell.transform.localPosition = new Vector3(-0.413f, 2.6581f, 8.541f);
                    AudioSource.PlayClipAtPoint(doorBell, bell.transform.position);
                    AudioSource.PlayClipAtPoint(doorSound, this.transform.position);
                    AudioSource.PlayClipAtPoint(puzzleComplete, this.transform.position);
                }


                if (!triedToOpen && !doorBellPickedUp)
                {
                    Debug.Log("Nimbi tried To open");
                    DialogueManager.Add(missingBell.Localize(), dialogueLength);
                    triedToOpen = true;
                }
                else{
                    DialogueManager.Add(watchYourself.Localize(), dialogueLength);
                }
            }
        }

        private void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
            {
                if (doorUnlocked)
                {
                       
                }


            }

        }
    }
}