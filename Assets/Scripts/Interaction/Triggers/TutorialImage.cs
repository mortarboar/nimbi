﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Interaction.Triggers
{
    [RequireComponent(typeof(Collider))]
    public class TutorialImage : MonoBehaviour
    {

        public GameObject image;

        void Start()
        {
            image.SetActive(false);
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            {
                image.SetActive(true);
                StartCoroutine(RemoveImage());
            }


        }

        IEnumerator RemoveImage()
        {
            yield return new WaitForSeconds(3f);
            Destroy(image);
            Destroy(this);
        }
    }
}