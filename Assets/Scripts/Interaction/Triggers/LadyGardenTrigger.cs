﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;
using Nimbi.CameraControl;
using Nimbi.UI;



namespace Nimbi.Interaction.Triggers {
public class LadyGardenTrigger : MonoBehaviour
{
    public AudioClip audioGreeting;
    public Animator ladyAnimator;

    private GameObject mainCamera;
    private Transform originalCameraPos;
    private Transform centerFocus;
    private Transform ladyGardenFocus;
    private bool dialoguePlaying = false;
    
    public string helloDialogue;
    
    public float dialogueLength;
    
    
    

    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        centerFocus = GameObject.FindGameObjectWithTag("MainCamera").transform.parent;
        ladyGardenFocus = GameObject.Find("CutsceneFocus").transform;
    }

    void Update()
    {
        if (dialoguePlaying)
        {
            centerFocus.Rotate(Vector3.down * Time.deltaTime);

            if (Input.GetButton("Submit"))
            {
                StopCoroutine("DisableMovement");

                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().EnableMovement();
                mainCamera.transform.localPosition = originalCameraPos.transform.localPosition;
                mainCamera.transform.localRotation = new Quaternion(0, 0, 0, 0);
                mainCamera.GetComponent<MainCameraScript>().cameraEnabled = true;
                ladyAnimator.SetBool("isTalking",false);
                Destroy(this);
            }
        }
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player")
        {
            mainCamera.GetComponent<MainCameraScript>().degree = 354;
            StartCoroutine("DisableMovement");
            //AudioSource.PlayClipAtPoint(audioGreeting, ladyGardenFocus.position);
            DialogueManager.Add(helloDialogue.Localize(), dialogueLength, Color.green);
            ladyAnimator.SetBool("isTalking", true);
        }
    }

    private void OnTriggerStay(Collider coll)
    {
        if (GameObject.Find("CenterFocus").transform.localRotation.eulerAngles.y >= 353 &&
            GameObject.Find("CenterFocus").transform.localRotation.eulerAngles.y <= 355)
        {
            originalCameraPos = mainCamera.transform;
            mainCamera.transform.localPosition = new Vector3(0, 0, -11);
            mainCamera.GetComponent<MainCameraScript>().cameraEnabled = false;
            mainCamera.transform.localPosition = new Vector3(0, 0, -11);
            mainCamera.transform.LookAt(ladyGardenFocus);
            dialoguePlaying = true;
        }
    }

    IEnumerator DisableMovement()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().DisableMovement();
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().Animator.SetBool("Moving", false);

        yield return new WaitForSeconds(12);
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().EnableMovement();
        mainCamera.transform.localPosition = originalCameraPos.transform.localPosition;
        mainCamera.transform.localRotation = new Quaternion(0, 0, 0, 0);
        ladyAnimator.SetBool("isTalking", false);
        mainCamera.GetComponent<MainCameraScript>().cameraEnabled = true;

        Destroy(this);
    }
}
    
}
