﻿using UnityEngine;
using System.Collections;
using Nimbi.Actors;
using Nimbi.UI;

namespace Nimbi.Interaction.Triggers
{
    public class MoleTrigger : MonoBehaviour
    {
        public Animator mole;
        public string waitDialogue;
        public string ringDialogue;
        public string getToGary;
        public string bellDialogue;
        public float dialogueLength;
        public AudioClip moleSound;

        public GameObject image;


        public void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            {
                StartCoroutine("DisableMovement");
                mole.SetBool("DiggingUp", true);
                DialogueManager.Add(waitDialogue.Localize(), dialogueLength);
               
                AudioSource.PlayClipAtPoint(moleSound, mole.transform.position);

                DialogueManager.Add(ringDialogue.Localize(), dialogueLength);
                
                if(GameObject.Find("Door").GetComponent<TutorialDoorTrigger>().doorBellPickedUp == false)
                {
                    DialogueManager.Add(bellDialogue.Localize(), dialogueLength);
                } else {
                    DialogueManager.Add(getToGary.Localize(), dialogueLength);
                }
                
            }
        }
        

        IEnumerator DisableMovement()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().DisableMovement();
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().Animator.SetBool("Moving", false);
            yield return new WaitForSeconds(3);

            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().EnableMovement();

            Destroy(gameObject);
        }
    }

}