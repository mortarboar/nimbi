﻿using UnityEngine;
using Nimbi.UI;

namespace Nimbi.Interaction.Triggers.Level1
{
    public class GaryInteractionTrigger : MonoBehaviour
    {

        private bool dialoguePlaying = false;

        void Update()
        {
            if (dialoguePlaying)
            {
                DialogueManager.Add("gary.go_on".Localize(), 1.2f);
                dialoguePlaying = false;
            }
        }

        private void OnTriggerStay(Collider coll)
        {
            if (Input.GetButtonDown("Submit"))
                if (coll.tag == "Player")
                    dialoguePlaying = true;
        }
    }
}