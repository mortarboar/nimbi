﻿using UnityEngine;
using System.Collections;

namespace Nimbi.Interaction.Triggers
{
    public class TutorialDoorBellTrigger : MonoBehaviour
    {

        private void OnTriggerStay(Collider coll)
        {
            if (coll.tag == "Player")
            {
                if (Input.GetButtonDown("Submit"))
                {
                    this.transform.parent = coll.transform;
                    this.transform.localPosition = new Vector3(0, 2.5f);
                    GameObject.Find("Door").GetComponent<TutorialDoorTrigger>().doorBellPickedUp = true;
                    /*GameObject.Find("BellIndicator").SetActive(true);
                    GameObject.Find("BellIndicator").GetComponent<Renderer>().materials = GetComponent<Renderer>().materials;*/

                    Destroy(this);
                }

            }
        }
    }
}