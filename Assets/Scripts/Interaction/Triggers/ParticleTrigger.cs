﻿using UnityEngine;
using System.Collections;


namespace Nimbi.Interaction.Triggers{
public class ParticleTrigger : MonoBehaviour {

public ParticleSystem particle;

    
    public void OnTriggerEnter(Collider coll){
        if(coll.tag == "Player"){
            particle.Play();
        }
    }
}
    
}
