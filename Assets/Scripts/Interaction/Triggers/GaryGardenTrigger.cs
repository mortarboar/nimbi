﻿using UnityEngine;
using Nimbi.Actors;
using Nimbi.UI;

namespace Nimbi.Interaction.Triggers
{
    public class GaryGardenTrigger : MonoBehaviour
    {
        public float dialogueLength;
        public string[] subtitles;

        public void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            {
                for (int i = 0; i < subtitles.Length; i++)
                {
                    DialogueManager.Add(subtitles[i].Localize(), dialogueLength);
                }
                Destroy(this);
            }
        }
    }
}