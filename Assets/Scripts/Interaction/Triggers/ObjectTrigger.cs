﻿using UnityEngine;
using System.Collections;

public class ObjectTrigger : MonoBehaviour {

    public bool isTriggered = false;

	private void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Player")
            isTriggered = true;
    }

    private void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "Player")
            isTriggered = false;
    }
}
