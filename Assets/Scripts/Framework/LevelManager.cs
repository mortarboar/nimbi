﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Nimbi.Framework {
	public class LevelManager : MonoBehaviour {
	
	public void LoadLevel(string name){
		SceneManager.LoadSceneAsync(name);
	}
	
	public void QuitRequest()
	{
		Application.Quit();
	}

        public void KickStart()
        {
            Application.OpenURL("https://kickstarter.com/projects/1442420975/nimbi-an-indie-puzzle-adventure-game");
        }

        public void LoadHomePage()
        {
            Application.OpenURL("http://www.nimbigame.com");
        }
      
    }
}

