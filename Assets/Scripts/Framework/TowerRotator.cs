﻿using UnityEngine;
using System.Collections;
using Nimbi.Interaction.Triggers;
using Nimbi.Framework;

namespace Nimbi.Interaction.Triggers
{
    public class TowerRotator : MonoBehaviour
    {
        public GameObject leverCamera;
        public GameObject Segment;
        public float rotationDegrees = 45f;
        public float rotationSpeed = 10f;
        public WindTrigger wind;
        public GameObject[] checkSegments;
        public float[] goalRotations;
        public Material newMaterial;
        public Material oldMaterial;
        public MeshRenderer buis;

        private GameObject mainCamera;

        private float nextRotation;
        private bool inTrigger;
        private bool leverActivated;
        public bool locked;

        void Start()
        {
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
            nextRotation = 0;
        }

        void Update()
        {
            if (!locked && wind.state == State.Active && Input.GetButtonDown("Submit") && inTrigger && !leverActivated)
            {
                leverActivated = true;
                nextRotation += rotationDegrees;
                nextRotation = nextRotation % 360;

                GameObject.FindGameObjectWithTag("Player").transform.parent = this.transform;

                leverCamera.SetActive(true);
                mainCamera.SetActive(false);
            }
            RotateTower();
            if (!locked)
                CheckRotations();
        }

        public void RotateTower()
        {
            if (leverActivated)
            {
                Segment.transform.localRotation = Quaternion.RotateTowards(Segment.transform.localRotation, Quaternion.Euler(0, nextRotation, 0), rotationSpeed * Time.deltaTime);

                if (Mathf.Approximately(nextRotation, Segment.transform.localRotation.eulerAngles.y))
                {
                    StartCoroutine(DelayCameraSwitch());
                    GameObject.FindGameObjectWithTag("Player").transform.parent = null;
                }

            }
        }

        private void CheckRotations()
        {
            bool rotationComplete = true;
            for (int s = 0; s < checkSegments.Length; s++)
            {
                if (!Mathf.Approximately(checkSegments[s].transform.localRotation.eulerAngles.y, goalRotations[s]))
                    rotationComplete = false;
            }

            if (rotationComplete)
            {
                locked = true;

                //Change material
                Material[] newMaterials = buis.materials;
                for (int m = 0; m < newMaterials.Length; m++)
                {
                    if (buis.materials[m].color == oldMaterial.color)
                    {
                        newMaterials[m] = newMaterial;
                    }
                }
                buis.materials = newMaterials;
            }
        }


        public void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = true;
        }

        public void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
                inTrigger = false;
        }

        IEnumerator DelayCameraSwitch()
        {
            yield return new WaitForSeconds(2.0f);
            mainCamera.SetActive(true);
            leverCamera.SetActive(false);
            leverActivated = false;
        }

    }

}
