﻿using Nimbi.Framework;
using SmartLocalization;
using UnityEngine;

namespace Nimbi
{
	// This class is in the root namespace on purpose!
	// Do not change
	public static class Extensions
	{

		static Extensions() {
			GameManager.Instance.Language = GameManager.Instance.Language;
		}

		/// <summary>
        /// Localizes a string by its key, e.g.: "string.to.localize".Localize()
        /// </summary>
        /// <param name="str">key of the string to localize</param>
        /// <returns>localized string</returns>
		public static string Localize(this string str)
		{
			string localized = LanguageManager.Instance.GetTextValue(str);
			return localized == null || localized == "" ? str : localized;
		}
	}
}

public static partial class AnimatorExtensions
{

    public static bool HasParameterOfType(this Animator self, string name, AnimatorControllerParameterType type)
    {
        var parameters = self.parameters;
        foreach (var currParam in parameters)
        {
            if (currParam.type == type && currParam.name == name)
            {
                return true;
            }
        }
        return false;
    }

}