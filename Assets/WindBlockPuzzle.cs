﻿using UnityEngine;
using Nimbi.Actors;

public class WindBlockPuzzle : MonoBehaviour
{
    public ObjectTrigger boundary;

    void Update()
    {
        if (!boundary.isTriggered)
            transform.Translate(Vector3.forward * Time.deltaTime);
    }

    private void OnCollisionStay(Collision coll)
    {
        if (coll.gameObject.tag == "Player")
            if (Player.Onesie.canMoveObjects)
                GetComponent<Rigidbody>().isKinematic = false;
            else
                GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnCollisionExit(Collision coll)
    {
        GetComponent<Rigidbody>().isKinematic = false;
    }
}
